#ifndef SCREEN_H
#define SCREEN_H

#include <Canvas.h>

class Screen {
	
private:

	std::function<void()>* needRefreshListener;


protected:
	
	void onNeedRefresh();
	

public:

	Screen();

	void setNeedRefreshListener(std::function<void()>* needRefreshListener);

	virtual void draw(Canvas& canvas) = 0;

};


#endif //SCREEN_H
