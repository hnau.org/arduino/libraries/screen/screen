#ifndef SCREENSSTACK_H
#define SCREENSSTACK_H

#include <Screen.h>
#include <ScreensRouter.h>
#include <TaggedLogger.h>

template <typename SCREEN>
class ScreensStack: public ScreensRouter<SCREEN>, public Screen {
	
private:

	TaggedLogger logger;
	
	SCREEN** stack;
	size_t maxDepth;
	size_t stackSize;
	std::function<void()> needRefreshListener;

public:

	ScreensStack(
		size_t maxDepth,
		Logger& logger_
	): needRefreshListener(
		[&]() { onNeedRefresh(); }
	), logger(logger_, FCP("ScreensStack")) {
		this->maxDepth = maxDepth;
		stack = new SCREEN*[maxDepth];
		stackSize = 0;
	}
	
	~ScreensStack() {
		free(stack);
	}
	
	SCREEN& getCurrentScreen() {
		return *(stack[stackSize-1]);
	}
	
	virtual void draw(Canvas& canvas) override {
		getCurrentScreen().draw(canvas);
	}

	virtual void forward(SCREEN* screen) override {
		
		if (stackSize >= maxDepth) {
			logger.log([&](Logger& log){
				log.log(FCP("Unable to go Forward. Stack is full, size: "));
				log.log(stackSize);
			});
			return;
		}
		
		if (stackSize >= 1) {
			stack[stackSize-1]->setNeedRefreshListener(nullptr);
		}
		stack[stackSize] = screen;
		stackSize++;
		screen->setNeedRefreshListener(&needRefreshListener);
		
		#ifdef DEBUG
			logger.log([&](Logger& log){
				log.log(FCP("Forward. Stack size: "));
				log.log(stackSize);
			});
		#endif
	}
	
	virtual bool back() override {
		if (stackSize <= 1) {
			logger.log([&](Logger& log){
				log.log(FCP("Unable to go Back. Stack size: 1"));
			});
			return false;
		}
		stack[stackSize-1]->setNeedRefreshListener(nullptr);
		stackSize--;
		stack[stackSize-1]->setNeedRefreshListener(&needRefreshListener);
		
		#ifdef DEBUG
			logger.log([&](Logger& log){
				log.log(FCP("Back. Stack size: "));
				log.log(stackSize);
			});
		#endif
		return true;
	}

};


#endif //SCREENSSTACK_H
