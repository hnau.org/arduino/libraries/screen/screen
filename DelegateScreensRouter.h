#ifndef SCREENSROUTER_H
#define SCREENSROUTER_H

#include <Screen.h>

class ScreensRouter {
	
public:

	virtual void forward(Screen& screen) = 0;
	virtual bool back() = 0;

};


#endif //SCREENSROUTER_H
