#include "Screen.h"


Screen::Screen() {
	needRefreshListener = nullptr;
}

void Screen::onNeedRefresh() {
	if (needRefreshListener != nullptr) {
		(*needRefreshListener)();
	}
}

void Screen::setNeedRefreshListener(std::function<void()>* needRefreshListener) {
	this->needRefreshListener = needRefreshListener;
	onNeedRefresh();
}
