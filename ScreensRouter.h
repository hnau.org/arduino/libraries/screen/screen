#ifndef SCREENSROUTER_H
#define SCREENSROUTER_H

#include <Screen.h>

template <typename SCREEN>
class ScreensRouter {
	
public:

	virtual void forward(SCREEN* screen) = 0;
	virtual bool back() = 0;

};


#endif //SCREENSROUTER_H
